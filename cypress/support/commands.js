import 'cypress-localstorage-commands'

Cypress.Commands.add('getBySel', (selector, ...args) => {
  return cy.get(`[data-test=${selector}]`, ...args)
})

Cypress.Commands.add('login', (username, password) => {
  const loginPath = '/login'

  cy.location('pathname', { log: false }).then((currentPath) => {
    if (currentPath !== loginPath) {
      cy.visit(loginPath)
    }
  })

  cy.getBySel('signin-email-input').clear().type(username)
  cy.getBySel('signin-password-input').clear().type(password)
  cy.getBySel('signin-button').click()
})

Cypress.Commands.add('loginAndSkipOnboarding', (email, password) => {
  cy.visit('/')

  // Login
  cy.login(email, password)

  // Language selector
  cy.getBySel('slide').should('be.visible')
  cy.getBySel('language-button').should('be.visible')
  cy.getBySel('continue-button').click()

  // Onboarding
  cy.location('pathname').should('equal', '/onboarding')
  cy.getBySel('heading-text').should('contain', 'Your new Aircall experience')
  cy.getBySel('copy').should('contain', 'Work more efficiently')
  cy.getBySel('next-button').click()
  cy.getBySel('copy').should('contain', 'Prioritize your calls')
  cy.getBySel('next-button').click()
  cy.getBySel('copy').should('contain', 'Search for past calls')
  cy.getBySel('next-button').click()
  cy.getBySel('copy').should('contain', 'Collaborate efficiently')
  cy.getBySel('next-button').click()
})
