before(() => {
  cy.loginAndSkipOnboarding(Cypress.env('testUser1').email, Cypress.env('testUser1').password)
  cy.saveLocalStorage()
})

beforeEach(() => {
  cy.restoreLocalStorage()
  cy.visit(Cypress.env('path').people_url)
})

describe('People Tab', () => {
  it('should open when click on it', function () {
    cy.getBySel('contacts-search-input').should('be.visible')
    cy.getBySel('people-contacts').should('be.visible')
    cy.getBySel('teammates-section').should('be.visible')
    cy.getBySel('item-button').its('length').should('be.gt', 0)
  })

  it('should search for a contact', () => {
    cy.getBySel('contacts-search-input').should('be.visible').type('QA')
    cy.getBySel('paragraph-text').contains('QA tester')
  })
})
