before(() => {
  cy.loginAndSkipOnboarding(Cypress.env('testUser1').email, Cypress.env('testUser1').password)
  cy.saveLocalStorage()
})

beforeEach(() => {
  cy.restoreLocalStorage()
})

describe('TODO Tab', () => {
  it('should open when click on it', function () {
    cy.visit(Cypress.env('path').todo_url)
    cy.location('pathname').should('equal', Cypress.env('path').todo_url)
    cy.getBySel('heading-text').should('contain', 'To-do')
    cy.getBySel('archive-all-button').should('be.visible')
    cy.getBySel('call-card').should('be.visible')
  })
})
