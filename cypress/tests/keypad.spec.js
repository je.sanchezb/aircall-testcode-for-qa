before(() => {
  cy.loginAndSkipOnboarding(Cypress.env('testUser1').email, Cypress.env('testUser1').password)
  cy.saveLocalStorage()
})

beforeEach(() => {
  cy.restoreLocalStorage()
  cy.visit(Cypress.env('path').keyboard_url)
})

describe('Keypad Tab', () => {
  it('should open when click on it', () => {
    cy.getBySel('keyboard-input-text').should('be.visible')
    cy.getBySel('keyboard-key-1').should('be.visible')
    cy.getBySel('keyboard-button-dial').should('be.visible')
    cy.getBySel('tab-bar').should('be.visible')
  })

  it('should be able to search for a contact', () => {
    cy.getBySel('keyboard-input-text').type('qa')
    cy.getBySel('people-search-teammates-section').should('be.visible')
    cy.getBySel('item-button').should('be.visible')
    cy.getBySel('item-button').click()
    cy.getBySel('keyboard-input-text').should('have.value', 'qa')
  })

  it('should be able to call a valid number using keypad', () => {
    cy.getBySel('keyboard-input-flag').click()
    cy.getBySel('search-field').type('Spain')
    cy.getBySel('paragraph-text').click()
    cy.getBySel('keyboard-input-text').type('627039514')
    cy.getBySel('keyboard-button-dial').click()

    // To ensure the communication is created I intercept the message after the comm is created in Twilio
    cy.intercept('POST', 'https://eventgw.twilio.com/v4/EndpointEvents', {
      statusCode: 200,
      body: {
        group: 'ice-candidate',
        name: 'ice-candidate'
      }
    }).as('connection')

    cy.wait('@connection').then(() => {
      cy.getBySel('hangup-button').click()
    })
  })

  it('should be able to change the phone number', () => {
    cy.getBySel('button-line').click()
    cy.getBySel('line-picker-panel').should('be.visible').then(() => {
      cy.getBySel('item-button').should('contain', Cypress.env('testUser1').phone)
      cy.getBySel('item-button').click()
    })
    cy.getBySel('line-number').should('contain', Cypress.env('testUser1').phone)
  })
})
