describe('User Login', () => {
  it('should redirect unauthenticated user to login page', function () {
    cy.visit(Cypress.env('path').keyboard_url)
    cy.location('pathname').should('equal', Cypress.env('path').login_url)
  })

  it('should allow an user to login, and logout"', () => {
    const user = Cypress.env('testUser1')
    cy.visit('/')

    // Login
    cy.login(user.email, user.password)

    // Language selector
    cy.getBySel('slide').should('be.visible')
    cy.getBySel('language-button').should('be.visible')
    cy.getBySel('continue-button').click()

    // Onboarding
    cy.location('pathname').should('equal', Cypress.env('path').onboarding_url)
    cy.getBySel('heading-text').should('contain', 'Your new Aircall experience')
    cy.getBySel('copy').should('contain', 'Work more efficiently')
    cy.getBySel('next-button').click()
    cy.getBySel('copy').should('contain', 'Prioritize your calls')
    cy.getBySel('next-button').click()
    cy.getBySel('copy').should('contain', 'Search for past calls')
    cy.getBySel('next-button').click()
    cy.getBySel('copy').should('contain', 'Collaborate efficiently')
    cy.getBySel('next-button').click()

    // Keyboard
    cy.location('pathname').should('equal', Cypress.env('path').keyboard_url)
    cy.getBySel('keyboard-input-text').should('be.visible')
    cy.getBySel('keyboard-key-1').should('be.visible')
    cy.getBySel('keyboard-button-dial').should('be.visible')
    cy.getBySel('tab-bar').should('be.visible')
  })

  it('should error for an invalid user', () => {
    cy.login('invalidUserName', 'invalidPa$$word')

    cy.getBySel('toast-destructive')
      .should('be.visible')
      .and('have.text', 'Failed to sign in')
  })
})
