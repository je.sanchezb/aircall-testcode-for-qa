/* eslint-disable no-unused-expressions */

before(() => {
  cy.loginAndSkipOnboarding('je.sanchezb@gmail.com', 'P23zB!a)')
  cy.saveLocalStorage()
})

beforeEach(() => {
  cy.restoreLocalStorage()
  cy.visit(Cypress.env('path').history_url)
})

describe('History Tab', () => {
  it('should open when click on it', function () {
    cy.getBySel('search-input').should('be.visible')
    cy.getBySel('call-card').should('be.visible')
  })

  it('should open a missing call', () => {
    cy.getBySel('call-card').first().click()
    cy.getBySel('call-details-fetched').should('be.visible')
    cy.getBySel('call-contact').should('be.visible')
    cy.getBySel('call-details-info').should('be.visible')
    cy.getBySel('via-number').should('be.visible')
    cy.getBySel('title-text').should('be.visible')
  }) 
})
