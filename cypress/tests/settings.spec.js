/* eslint-disable no-unused-expressions */

before(() => {
  cy.loginAndSkipOnboarding(Cypress.env('testUser1').email, Cypress.env('testUser1').password)
  cy.saveLocalStorage()
})

beforeEach(() => {
  cy.restoreLocalStorage()
  cy.getBySel('tab-bar-settings').click()
})

afterEach(() => {
  cy.visit('/')
})

describe('Settings Tab', () => {
  it('should open when click on it', () => {
    cy.getBySel('panel').should('be.visible')
    cy.getBySel('settings-menu-account').should('be.visible')
    cy.getBySel('settings-menu-sounds-devices').should('be.visible')
    cy.getBySel('settings-menu-call-preferences').should('be.visible')
    cy.getBySel('settings-menu-availability-schedule').should('be.visible')
    cy.getBySel('settings-menu-email-notifications').should('be.visible')
    cy.getBySel('settings-menu-help').should('be.visible')
    cy.getBySel('settings-menu-refer-friend').should('be.visible')
    cy.getBySel('settings-menu-logout').should('be.visible')
    cy.getBySel('available').should('be.visible')
    cy.getBySel('auto').should('be.visible')
    cy.getBySel('unavailable').should('be.visible')
  })

  it('should open account', () => {
    cy.getBySel('settings-menu-account').click()
    cy.getBySel('contact-name').should('be.visible')
    cy.getBySel('paragraph-text').should('be.visible')
    cy.getBySel('set-edit-action').should(($btn) => {
      expect($btn).to.be.visible
      expect($btn).to.be.enabled
    })

    cy.getBySel('settings-account-credentials').should('be.visible')
    cy.getBySel('settings-account-languages').should('be.visible')
    cy.getBySel('settings-account-lines').should('be.visible')
  })

  it('should be able to logout and login', () => {
    cy.intercept('DELETE', 'https://id.aircall.io/auth/v0/users/session', {
      statusCode: 204
    }).as('connection')

    cy.getBySel('settings-menu-logout').click()
    cy.location('pathname').should('equal', Cypress.env('path').login_url)
    const user = Cypress.env('testUser1')
    cy.wait('@connection')
    cy.login(user.email, user.password)
    cy.location('pathname').should('equal', '/')
  })
})
