# Test code - QA - Aircall

## About this repo

This repository contains the solutions for the [QA Backend](https://bitbucket.org/doucetm/qa-test/src/master/test-back.md) and [QA Frontend Senior](https://bitbucket.org/doucetm/qa-test/src/master/test-front-senior) test cases for Aircall.

In this repo you can find:

- Test Plan for frontend and backend
- Automation for each technology
- Test Execution using Gitlab CI

## Test Plan

All the tests plans are in [doc](doc). Each test plan contains some preliminary remarks, the features under testing, the tests cases and the file where is automated.

## Automation

The language is used for automation (both frontend and backend) is NodeJS.

### Backend

I used [Postman](https://www.postman.com/) to automate the backend. Why Postman? Postman is a  collaboration platform for API development, is easy to use, has a good interface, and allow [API testing](https://www.postman.com/use-cases/api-testing-automation/) using Javascript and integration with CI/CD systems.

To save the collections, you have different options: save the collections online and share them with your team, or save the collections as a JSON document and include it into your repo. For this test code I've chosen the second option.

In this repo you can find two different docs:

- Tests: `tests/api/search_mobile.json`
- Config: `api_env.json`

The first one contains the API Request, and the tests while the second one contains all the config (url, path, version...)

You can execute it in two different ways:

#### Using Postman

In this case you only have to open the JSON files with Postman and execute it. You can follow this [tutorial](https://learning.postman.com/docs/running-collections/working-with-data-files/)

#### Using Terminal

You need to install NodeJS (you can follow this [tutorial](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm/)) and simply run this:

```bash
npm install
yarn run test:backend  
```

### Frontend

To automate the fronted I've chosen [Cypress](https://www.cypress.io/) to avoid all the problems with waitings and selectors than exist in Selenium.

I have used **neither cucumber nor page object**. The main reason has been to avoid over-engineering. The automated test cases are smoke tests that executed one or two different actions on the different features so creating a page object for this case would be unnecessary, and defining the test cases as features instead of steps would not improve readability and would add an unnecessary layer of complexity to the code.

To execute the tests you only have to do:

```bash
npm install
yarn run test:frontend
```

Every execution will return a video into `cypress/videos` folder with all the executions and retries. And, if there is an error during execution, the system will take a screenshot and it will save it into `cypress/screenshots`.

## Executions

After each push to remote or Merge Request all the tests will be executed in Gitlab using a Docker image (you can check the config into [.gitlab.ci-yml](.gitlab-ci.yml)).

You can re-run the tests in every moment simply using `Run Pipeline` in the `CI/CD` option.

## Bugs

During the execution I've found an issue in the documentation that I've been reported [here](https://gitlab.com/je.sanchezb/aircall-testcode-for-qa/-/issues/1).
