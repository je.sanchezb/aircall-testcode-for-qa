# Test plan for backend

This test plan consists of testing the API call to look up a phone number. The API request is:

```
https://api.aircall.io/v1/contacts/search?order=asc&order_by=created_at&phone_number=[number]
```

And the phone number to test is:

```
+33652556756
```

## Preliminary remarks

For the sake of simplicity and since it consists of only one functionality to be tested, the test plan will consist of a single document with all the test cases.  

We use the schema to validate the response from the [doc](https://developer.aircall.io/api-references/#search-contacts). We will assume that marked fields that are not marked with null are mandatory fields.

```json
{
    "meta" : {
        "count" : { "type": "integer" },
        "total" : { "type": "integer" },
        "current_page" : { "type": "integer" },
        "per_page" : { "type": "integer" },
        "next_page_link" : { "type": ["null", "string"] },
        "previous_page_link" : { "type": ["null", "string"] },
    },
    "contacts": {
        "type": "array",
        "items": {
            "id": { "type": "integer" } ,
            "direct_link":  { "type": "string" },
            "first_name": { "type": "string" },
            "last_name": { "type": "string" },
            "company_name": { "type": ["string", "null"] },
            "information": { "type": ["string", "null"] },
            "is_shared": {"type": "boolean"},
            "created_at": { "type": "integer" },
            "updated_at": { "type": "integer" },
            "emails": {
                "type": "array",
                "items": {
                    "id": { "type": "integer" },
                    "label": { "type": "string" },
                    "value": { "type": "string" }
                }    
            },
            "phone_numbers": {
                "type": "array",
                "items": {
                    "id": { "type": "integer" },
                    "label": { "type": "string" },
                    "value": { "type": "string" }
                }
            }
        }
    }
}      
```

## Test cases

ID | Description | Expected Result  
--- | --- | ---
1 | Status code is 200 | Response has to be 200 OK
2 | Schema is valid | Schema for the response fit with the schema defined in the documentation
3 | There is only one contact with the phone number | The number of elements in `contacts` field has to be one and it has to match with the `meta` data
4 | ID is valid | Id has to be positive and bigger than 0
5 | ID appears in the `direct_link` | The `direct_link` has to have the ID
6 | The mobile phone appears into phone numbers array only once | The mobile phone we are searching has to appear only once in the array of `phone_numbers`
