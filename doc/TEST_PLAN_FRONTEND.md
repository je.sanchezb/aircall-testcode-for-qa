# Test plan for frontend (senior)

In this test plan we will focus on defining the basic test cases for a Smoke Test that would be performed during a release process to ensure that the basic functionality continues to work.

We will focus on basic tests for each of the features of the app.

## Preliminary remarks

These tests will not cover all possible regression for each of the features and will focus on:

1. We check that each tab is functional and contains the necessary elements
2. Calls can be made to any phone number as the validation will be done using Twilio requests. I do that to avoid hacks or scripts to execute two different browsers with two different users.
3. Only the user profile will be checked within the settings.

## Features

### Login

#### Pre-conditions

- Has to exists a valid user

#### Test Cases

ID | Description | Data | Expected result | Automated Test
--- | --- | --- | --- | ---
LOG-1 | Login with valid credentials | je.sanchezb@gmail.com - P23zB!a) | User has to login and onboarding has to appear | [login.spec.js](../cypress/tests/login.spec.js)
LOG-2 | Login with invalid credentials | je.sanchezb@gmail.com - Invalid | Login error has to appear in the form | [login.spec.js](../cypress/tests/login.spec.js)


### TO-DO

#### Pre-conditions

- The user has to be logged into the app, and the onboarding has to be done.
- The user has tasks to do.

#### Test Cases

ID | Description | Data | Expected result | Automated Test
--- | --- | --- | --- | ---
TODO-1 | Check the tab opens and there are data into the form | - | The tab is open and there are missing calls | [todo.spec.js](../cypress/tests/todo.spec.js)

### History

#### Pre-conditions

- The user has to be logged into the app, and the onboarding has to be done.
- The user has missing calls.

#### Test Cases

ID | Description | Data | Expected result | Automated Test
--- | --- | --- | --- | ---
HIST-1 | Check the tab opens and there are missing calls | - | The tab is open and there is missing calls | [history.spec.js](../cypress/tests/history.spec.js)
HIST-2 | Open a missing call | First missing call | The missing call opens and Contact Info, Call Info, Call History, and Call Back button appears |[history.spec.js](../cypress/tests/history.spec.js) 

### Keypad

#### Pre-conditions

- The user has to be logged into the app, and the onboarding has to be done.
- The user has as contact to QA Tester

#### Test Cases

ID | Description | Data | Expected result | Automated Test
--- | --- | --- | --- | ---
KEY-1 | Check the tab opens and keypad appears | - | The tab opens and the keypad, phone line, and call button appear | [keypad.spec.js](../cypress/tests/keypad.spec.js)
KEY-2 | Search a contact | QA | The contact appears in the Keypad tab and is callable | [keypad.spec.js](../cypress/tests/keypad.spec.js)
KEY-3 | Call a phone number using keypad | +34 627 039 514 | A connection has to be stablished with this phone | [keypad.spec.js](../cypress/tests/keypad.spec.js)
KEY-4 | Change phone line | - | The user can select the phone line | [keypad.spec.js](../cypress/tests/keypad.spec.js)

### People

- The user has to be logged into the app, and the onboarding has to be done.
- The user has as contact to QA Tester

#### Test Cases

ID | Description | Data | Expected result | Automated Test
--- | --- | --- | --- | ---
PEOP-1 | Search a contact | QA | The contact appears in the Keypad tab and is callable | [keypad.spec.js](../cypress/tests/keypad.spec.js)

### Settings

- The user has to be logged into the app, and the onboarding has to be done.

#### Test Cases

ID | Description | Data | Expected result | Automated Test
--- | --- | --- | --- | ---
SETT-1 | Settings form has to be visible | - | The settings form has to be visible and has to have Account, Sounds and Devices, Call Preferences, Working Hours, Email Notification, Support, Refer a friend and Logout | [settings.spec.js](../cypress/tests/settings.spec.js)
SETT-2 | Account has to open | - | Name, Company, Credentials, Language, and Number has to be visible | [settings.spec.js](../cypress/tests/settings.spec.js)
